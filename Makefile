
PROGRAM_NAME := uniparser

SRCS = grammar.c
OBJS = $(SRCS:.c=.o)

.PHONY: all clean dynamic static

all: static

exec: $(PROGRAM_NAME)

dynamic: lib$(PROGRAM_NAME).so

static: lib$(PROGRAM_NAME).a

$(PROGRAM_NAME): lib$(PROGRAM_NAME).a main.c
	$(CC) -o $@ $^

lib$(PROGRAM_NAME).a: $(OBJS)
	ar rcs $@ $^

lib$(PROGRAM_NAME).so: $(SRCS)
	$(CC) -fPIC -c $^
	$(CC) -shared -o $@ $(OBJS)

clean:
	rm -rf $(PROGRAM_NAME)
	rm -rf lib$(PROGRAM_NAME).a
	rm -rf lib$(PROGRAM_NAME).so
	rm -rf *.o
